package hts_studios.coluanimation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 * Animation (comes first in the alphabet)
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize view variables
        final LinearLayout upperImageLayout = (LinearLayout) findViewById(R.id.upper_user_image);
        LinearLayout bottomImageLayout = (LinearLayout) findViewById(R.id.bottom_user_image);
        FrameLayout arrowLayout = (FrameLayout) findViewById(R.id.central_arrow_transfer_layout);
        final View whiteIndicator = findViewById(R.id.white_indicator);
        TextView senderName = (TextView) findViewById(R.id.upper_text_name);
        TextView receiverName = (TextView) findViewById(R.id.bottom_text_name);

        //create arrow runner Object animator
        ObjectAnimator translationY = ObjectAnimator.ofFloat(whiteIndicator, View.TRANSLATION_Y, -40, getResources().getDimension(R.dimen.arrow_length));
        translationY.setRepeatCount(3);
        translationY.setDuration(1000);
        //fade out arrow when done
        ObjectAnimator arrowFadeOut = ObjectAnimator.ofFloat(arrowLayout, View.ALPHA, 0f);
        arrowFadeOut.setDuration(800);
        //move sender userpic down to the center of the screen
        ObjectAnimator translateUpperImageDown = ObjectAnimator.ofFloat(upperImageLayout, View.TRANSLATION_Y, calcDownTransitionPath());
        long TRANSITION_DURATION = 600;
        translateUpperImageDown.setDuration(TRANSITION_DURATION);
        translateUpperImageDown.setInterpolator(new AccelerateInterpolator());
        translateUpperImageDown.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }
            //at animation end hide sender userpic so it wont be visible at receiver rotation
            @Override
            public void onAnimationEnd(Animator animator) {
                if (upperImageLayout != null)
                    upperImageLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        //fade out sender name field
        ObjectAnimator senderNameFadeOut = ObjectAnimator.ofFloat(senderName, View.ALPHA, 0f);
        senderNameFadeOut.setDuration(TRANSITION_DURATION);
        senderNameFadeOut.setInterpolator(new AccelerateInterpolator());
        //move receiver userpic up to the center of the screen
        ObjectAnimator translateBottomImageUp = ObjectAnimator.ofFloat(bottomImageLayout, View.TRANSLATION_Y, -calcUpTransitionPath());
        translateBottomImageUp.setDuration(TRANSITION_DURATION);
        translateBottomImageUp.setInterpolator(new AccelerateInterpolator());
        //fade out the receiver name text field
        ObjectAnimator receiverNameFadeOut = ObjectAnimator.ofFloat(receiverName, View.ALPHA, 0f);
        receiverNameFadeOut.setDuration(TRANSITION_DURATION);
        receiverNameFadeOut.setInterpolator(new AccelerateInterpolator());
        //fade in all the hidden success messages
        TextView successfulDoneMessage = (TextView) findViewById(R.id.done_success_message);
        ObjectAnimator doneTextFadeIn = ObjectAnimator.ofFloat(successfulDoneMessage, View.ALPHA, 0, 0.5f);
        doneTextFadeIn.setInterpolator(new AccelerateInterpolator());
        doneTextFadeIn.setDuration(TRANSITION_DURATION);

        TextView successfulUpperMessage = (TextView) findViewById(R.id.upper_success_message);
        ObjectAnimator upperSuccessFadeIn = ObjectAnimator.ofFloat(successfulUpperMessage, View.ALPHA, 0, 0.5f);
        upperSuccessFadeIn.setInterpolator(new AccelerateInterpolator());
        upperSuccessFadeIn.setDuration(TRANSITION_DURATION);

        TextView successfulBottomMessage = (TextView) findViewById(R.id.bottom_success_message);
        ObjectAnimator bottomSuccessFadeIn = ObjectAnimator.ofFloat(successfulBottomMessage, View.ALPHA, 0, 0.7f);
        bottomSuccessFadeIn.setInterpolator(new AccelerateInterpolator());
        bottomSuccessFadeIn.setDuration(TRANSITION_DURATION);
        //now the tricky rotation with source image replacement in the middle
        ObjectAnimator halfRotate = ObjectAnimator.ofFloat(bottomImageLayout, View.ROTATION_Y, 0.0f, 15f, 0.0f, 90f);
        halfRotate.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }
            //replace the image to transaction success mark
            @Override
            public void onAnimationEnd(Animator animator) {
                ImageView imageView = (ImageView) findViewById(R.id.bottom_image_src);
                if (imageView != null) {
                    imageView.setImageDrawable(null);
                    imageView.setImageResource(R.mipmap.success_mark);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        halfRotate.setDuration(800);

        //and another tricky rotate but with a new image already
        ObjectAnimator halfRotateBack = ObjectAnimator.ofFloat(bottomImageLayout, View.ROTATION_Y, -90f, 0.0f, 15.0f, 0.0f);
        halfRotateBack.setInterpolator(new AccelerateDecelerateInterpolator());
        halfRotateBack.setDuration(1000);
        /*
         * bringing all of them together in expected order with AnimatorSet
         */
        AnimatorSet sendingAnimator = new AnimatorSet();

        sendingAnimator.play(translationY);
        sendingAnimator.play(arrowFadeOut).after(translationY);
        sendingAnimator.play(translateUpperImageDown).after(translationY);
        sendingAnimator.play(translateBottomImageUp).after(translationY);
        sendingAnimator.play(senderNameFadeOut).after(translationY);
        sendingAnimator.play(receiverNameFadeOut).after(translationY);
        sendingAnimator.play(doneTextFadeIn).after(translationY);
        sendingAnimator.play(upperSuccessFadeIn).after(translationY);
        sendingAnimator.play(bottomSuccessFadeIn).after(translationY);

        sendingAnimator.play(halfRotate).after(translateBottomImageUp);
        sendingAnimator.play(halfRotateBack).after(halfRotate);

        sendingAnimator.start();
    }

    /*
     * Calculates the path length in pixels for the upper layout to translate
     * Consists of several factors: arrow height, userpic height, text size in text view,
     * layout padding and text view margin
     */
    private float calcDownTransitionPath() {
        return getResources().getDimension(R.dimen.arrow_length) / 2 + getResources().getDimension(R.dimen.userpic_outer_diameter) / 2 + getResources().getDimension(R.dimen.userpic_layout_padding)
                + getResources().getDimension(R.dimen.default_text_size) + getResources().getDimension(R.dimen.small_margin);
    }

    /*
     * Calculates the path length in pixels for the bottom layout to translate
     * will be a little shorter as upper path because of textView position
     * Consists of arrow height, userpic height and layout padding
     */
    private float calcUpTransitionPath() {
        return getResources().getDimension(R.dimen.arrow_length) / 2 + +getResources().getDimension(R.dimen.userpic_outer_diameter) / 2 + getResources().getDimension(R.dimen.userpic_layout_padding);
    }

}
